<?namespace Intervolga\Checklist;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use Intervolga\Checklist\Tests\Base;

Loc::loadMessages(__FILE__);

class EventHandlers
{
	/**
	 * Returns test classes
	 *
	 * @return Base[]
	 */
	protected static function getTestsClasses()
	{
		$arTests = array();
		if (file_exists(__DIR__ . "/tests/"))
		{
			$arFiles = scandir(__DIR__ . "/tests/");
			$arFiles = array_diff($arFiles, array(".", ".."));
			foreach ($arFiles as $file)
			{
				include_once (__DIR__ . "/tests/" . $file);
			}

			foreach (get_declared_classes() as $class)
			{
				if ( (is_subclass_of($class, "\\Intervolga\\Checklist\\Tests\\Base") && !strpos($class,"PhpFileBase")))
				{
					$arTests[] = $class;
				}
			}
		}

		return $arTests;
	}

	/**
	 * Returns module tests
	 *
	 * @param array $arCheckList
	 *
	 * @return array
	 */
	public static function onCheckListGet($arCheckList)
	{
		$arResult = array(
			"CATEGORIES" => array(
				"IV_BASE" => array(
					"NAME" => Loc::getMessage("intervolga.checklist.CATEGORY_IV_BASE"),
					"LINKS" => "",
				),
			),
			"POINTS" => array(
			)
		);
		foreach (self::getTestsClasses() as $test)
		{
			$arResult["POINTS"][$test::getCode()] = $test::getCheckListPoint();
		}
		$connection = Application::getConnection();
		$recordset = $connection->query("select * from reports");
		while ($record = $recordset->fetch())
		{
			$arCheckListPoint = array (
			"CLASS_NAME" => __CLASS__,
			"METHOD_NAME" => "check",
			"PARENT" => $record['parentName'],
			"NAME" => $record['name'],
			"REQUIRED" => "Y",
			"AUTO" => "Y",
			"DESC" => $record['description'],
			"PARAMS" => $record);
			$arResult["POINTS"][$record['code']] = $arCheckListPoint;
		}
		return $arResult;
	}
	
	static public function check($arParams)
    {
        if($arParams['status']!="N") 
		{
            $arResult = array(
                'STATUS' => true,
                'MESSAGE' => array(
                    'PREVIEW' => $arParams['preview'],
                ),
            );
        } 
		else 
		{
            $arResult = array(
                'STATUS' => false,
                'MESSAGE' => array(
                    'PREVIEW' => $arParams['preview'],
                    'DETAIL' => $arParams['details'],
                ),
            );
        }

        return $arResult;
    }
}