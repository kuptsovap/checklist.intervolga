<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

class DisplayError extends Base
{
	public static function getCode()
	{
		return "IV_DISPLAY_ERROR";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_DISPLAY_ERROR_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_DISPLAY_ERROR_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array();

		$display = ini_get('display_errors');
		if (!$display)
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.DISPLAY_OFF", array(
				"#PAGE#" => $file->getName()
			));
		}		

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DISPLAY_ERROR_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DISPLAY_ERROR_OK");
		}

		return $arResult;
	}
	
}