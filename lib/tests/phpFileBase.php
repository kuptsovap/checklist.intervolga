<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

abstract class PhpFileBase extends Base 
{	
	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array();
		$dir = new Directory(Application::getDocumentRoot());
		if ($arFiles = self::getTestFiles($dir))
		{
			foreach ($arFiles as $file)
			{
				$content = $file->getContents();
				if ($content)
				{
					$arErrors = array_merge($arErrors, static::checkContent($content, $file));
				}
				else
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.PAGE_UNAVAILABLE", array(
						"#PAGE#" => $file->getPhysicalPath()
					));
				}
			}
		}
		
		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = static::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.OK");
		}
		return $arResult;
	}
	
	/**
	 * Returns files to check
	 *
	 * @return file[]
	 */
	protected static function getTestFiles($dirname, $privateSearch = false, $extension = "php")
	{
		$fileList = array();
		if(!$dirname->isExists())
			return $fileList;
		$arFiles = $dirname->getChildren();
		if ($arFiles)
		{
			foreach ($arFiles as $file)
			{
					$notPrivateDir = !substr_count($file->getDirectoryName(),"www/bitrix") && !substr_count($file->getDirectoryName(),"www/local");
					if ($file->isDirectory() && ($notPrivateDir || $privateSearch))
					{
							$fileList = array_merge($fileList,self::getTestFiles($file,$privateSearch,$extension));
					}
					else
					{
						if ($file->isFile() && !strcmp($file->getExtension(),$extension) && ($notPrivateDir || $privateSearch))
						{
							$fileList[] = $file;
						}
					}
			}
		}

		return $fileList;
		
	}
	
	/**
	 * Returns errors on file
	 *
	 * @param string $content file content
	 * @param File $file file
	 *
	 * @return \string[]
	 */
	 protected static function checkContent($content, $file)
	 {

	 }
}