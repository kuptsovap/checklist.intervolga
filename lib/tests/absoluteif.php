<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class AbsoluteIf extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_ABSOLUTE_IF";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_ABSOLUTE_IF_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_ABSOLUTE_IF_TEST_DESC");
	}
	
	public static function run()
	{
		return parent::run();
	}

	protected static function checkContent($content, $file)
	{		
		$arErrors = array();

		if ($if0 = substr_count($content, "if(0)"))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.IF0_FOUND", array(
				"#CNT#" => $if0,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($if1 = substr_count($content, "if(1)"))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.IF1_FOUND", array(
				"#CNT#" => $if1,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($iffalse = substr_count($content, "if(false)"))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.IFFALSE_FOUND", array(
				"#CNT#" => $iffalse,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($iftrue = substr_count($content, "if(true)"))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.IFTRUE_FOUND", array(
				"#CNT#" => $iftrue,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}

		return $arErrors;
	}
}