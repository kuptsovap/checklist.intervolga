<?namespace Intervolga\Checklist\Tests;

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class TestFiles extends Base
{
	public static function getCode()
	{
		return "IV_TEST_FILES";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_TEST_FILES_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_TEST_FILES_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array();

		$arRootFiles = scandir($_SERVER["DOCUMENT_ROOT"]);
		foreach ($arRootFiles as $file)
		{
			if ($file != "." && $file != "..")
			{
				if (self::isTestFileOrDir($_SERVER["DOCUMENT_ROOT"] . "/" . $file))
				{
					if (is_dir($_SERVER["DOCUMENT_ROOT"] . "/" . $file))
					{
						$arErrors[] = Loc::getMessage("intervolga.checklist.IV_TEST_FILES_DIR_ERROR", array(
							"#HREF#" => "/bitrix/admin/fileman_admin.php?lang=" . LANG . "&path=" . urlencode($file),
							"#DIR#" => "/" . $file . "/",
						));
					}
					else
					{
						$arErrors[] = Loc::getMessage("intervolga.checklist.IV_TEST_FILES_FILE_ERROR", array(
							"#HREF#" => "/bitrix/admin/fileman_file_view.php?path=/".urlencode($file)."&lang=" . LANG,
							"#FILE#" => "/" . $file,
						));
					}
				}
			}
		}

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_TEST_FILES_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}

		return $arResult;
	}

	/**
	 * Checks if file is test
	 *
	 * @param string $file file path and name
	 *
	 * @return bool
	 */
	private static function isTestFileOrDir($file)
	{
		return
			substr_count($file, "test") > 0
			|| substr_count($file, "debug") > 0
			|| (substr_count($file, ".html") > 0 && substr_count($file, "500.html") != 1)
			|| substr_count($file, "tmp") > 0
			|| substr_count($file, "temp") > 0
			|| substr_count($file, "demo") > 0
			|| substr_count($file, "hidden") > 0
			|| substr_count($file, "import.php") > 0
			|| substr_count($file, "login.php") > 0
			|| substr_count($file, "bitrixsetup.php") > 0
			|| substr_count($file, "phpinfo") > 0
			|| substr_count($file, "restore.php") > 0;
	}
}