<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class PublicImages extends Base
{
	const MAX_IMAGES_TOTAL_SIZE = 2147483648;   // 2 Megabytes

	public static function getCode()
	{
		return "IV_PUBLIC_IMAGES";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_PUBLIC_IMAGES_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_PUBLIC_IMAGES_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arCheckResult = self::getResult();
		if ($arCheckResult["ERRORS"])
		{
			$arErrors = array_merge($arCheckResult["OK"], $arCheckResult["ERRORS"]);
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_PUBLIC_IMAGES_ERRORS_FOUND", array("#CNT#" => count($arCheckResult["ERRORS"])));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_PUBLIC_IMAGES_OK");
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arCheckResult["OK"]);
		}

		return $arResult;
	}

	/**
	 * Returns all pages check result
	 *
	 * @return array with keys ERRORS and OK
	 */
	private static function getResult()
	{
		$arResult = array(
			"ERRORS" => array(),
			"OK" => array(),
		);

		$arSites = SiteTable::getList(array(
			"select" => array("LID", "SERVER_NAME", "DIR")
		))->fetchAll();
		if ($arSites)
		{
			foreach ($arSites as $arSite)
			{
				$arPageResult = self::getPageCheckResult($arSite["SERVER_NAME"], $arSite["DIR"]);
				if ($arPageResult["ERROR"])
				{
					$arResult["ERRORS"][] = $arPageResult["ERROR"];
				}
				if ($arPageResult["OK"])
				{
					$arResult["OK"][] = $arPageResult["OK"];
				}
			}
		}

		return $arResult;
	}

	/**
	 * Checks page for image sizes
	 *
	 * @param string $server server name (example.com)
	 * @param string $page (site relative path)
	 *
	 * @return array with key ERROR or OK
	 */
	private static function getPageCheckResult($server, $page)
	{
		$arResult = array();
		$arImageSizes = array();
		$displayPage = $server . ($page== "/" ? "" : $page);
		$content = file_get_contents("http://" . $server . $page);
		if ($content)
		{
			$arResult = array();
			preg_match_all('/<img[^>]+>/i', $content, $arResult);
			if ($arResult[0])
			{
				foreach ($arResult[0] as $img)
				{
					$arSrc = array();
					preg_match_all('/src=("[^"]*"|\'[^\']*\')/i', $img, $arSrc);
					if ($arSrc)
					{
						$src = trim($arSrc[1][0], "'\"");
						$absoluteImagePath = "";
						if (substr_count($src, "://") == 0 && substr_count($src, "//") == 0)
						{
							$absoluteImagePath = "http://" . $server . $src;
						}
						elseif (substr_count($src, "://") == 1)
						{
							$absoluteImagePath = $src;
						}
						else
						{
							AddMessage2Log(__FILE__ . ":" . __LINE__ . "\n" . print_r("undefined src", TRUE) . "\n\n");
						}

						if ($absoluteImagePath)
						{
							$arHeaders = get_headers($absoluteImagePath, true);
							$arImageSizes[$absoluteImagePath] = $arHeaders["Content-Length"];
						}
					}
				}
			}
		}
		if ($arImageSizes)
		{
			if (array_sum($arImageSizes) > self::MAX_IMAGES_TOTAL_SIZE)
			{
				$arResult["ERROR"] = Loc::getMessage(
					"intervolga.checklist.IV_PUBLIC_IMAGES_TOO_LARGE",
					array(
						"#PAGE#" => $displayPage,
						"#URL#" => "http://" . $server . $page,
						"#SIZE#" => \CFile::FormatSize(array_sum($arImageSizes)),
						"#COUNT#" => count($arImageSizes)
					)
				);
			}
			else
			{
				$arResult["OK"] = Loc::getMessage(
					"intervolga.checklist.IV_PUBLIC_IMAGES_FITS",
					array(
						"#PAGE#" => $displayPage,
						"#URL#" => "http://" . $server . $page,
						"#SIZE#" => \CFile::FormatSize(array_sum($arImageSizes)),
						"#COUNT#" => count($arImageSizes)
					)
				);
			}
		}
		else
		{
			$arResult["OK"] = Loc::getMessage(
				"intervolga.checklist.IV_PUBLIC_IMAGES_NO_IMAGES",
				array(
					"#PAGE#" => $displayPage,
					"#URL#" => "http://" . $server . $page,
				)
			);
		}
		return $arResult;
	}
}