<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class DefaultTitles extends Base
{
	public static function getCode()
	{
		return "IV_DEFAULT_TITLES";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();

		$arErrors = array_merge(self::checkModulesSettings(), self::checkSitesSettings(), self::checkSectionsSettings(), self::checkPagesSettings());

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_OK");
		}

		return $arResult;
	}

	/**
	 * Returns errors in modules settings
	 *
	 * @return string[]
	 */
	private static function checkModulesSettings()
	{
		$arErrors = array();
		$siteName = Option::get("main", "site_name");
		if (self::isDefault($siteName))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_MODULE_SITE_NAME_DEFAULT", array(
				"#TITLE#" => htmlspecialchars($siteName),
				"#HREF#" => "/bitrix/admin/settings.php?mid=main&lang=" . LANG,
			));
		}
		return $arErrors;
	}

	/**
	 * Returns errors in sites settings
	 *
	 * @return string[]
	 */
	private static function checkSitesSettings()
	{
		$arErrors = array();
		$arSites = SiteTable::getList(array(
			"select" => array("LID", "SITE_NAME", "NAME"),
		))->fetchAll();
		foreach ($arSites as $arSite)
		{
			if (self::isDefault($arSite["NAME"]))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_SITE_NAME_DEFAULT", array(
					"#TITLE#" => htmlspecialchars($arSite["NAME"]),
					"#LID#" => $arSite["LID"],
					"#HREF#" => "/bitrix/admin/site_edit.php?lang=" . LANG ."&LID=" . $arSite["LID"],
				));
			}
			if (self::isDefault($arSite["SITE_NAME"]))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_MODULE_SITE_SITE_NAME_DEFAULT", array(
					"#TITLE#" => htmlspecialchars($arSite["SITE_NAME"]),
					"#LID#" => $arSite["LID"],
					"#HREF#" => "/bitrix/admin/site_edit.php?lang=" . LANG ."&LID=" . $arSite["LID"],
				));
			}
		}

		return $arErrors;
	}

	/**
	 * Returns errors in sections settings
	 *
	 * @return string[]
	 */
	private static function checkSectionsSettings()
	{
		$arErrors = array();
		$arSites = SiteTable::getList(array(
			"select" => array("LID", "DIR"),
		))->fetchAll();
		foreach ($arSites as $arSite)
		{
			$sectionSettingsFile = $_SERVER["DOCUMENT_ROOT"] . $arSite["DIR"] . ".section.php";
			if (file_exists($sectionSettingsFile))
			{
				include $sectionSettingsFile;
				/**
				 * @var string $sSectionName
				 * @var string[] $arDirProperties
				 */
				if ($sSectionName && self::isDefault($sSectionName))
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_PAGE_SECTION_DEFAULT", array(
						"#TITLE#" => htmlspecialchars($sSectionName),
						"#HREF#" => "/bitrix/admin/fileman_folder.php?lang=" . LANG . "&site=" . $arSite["LID"] . "&path=" . urlencode($arSite["DIR"]),
					));
				}
				if ($arDirProperties)
				{
					foreach ($arDirProperties as $dirProperty)
					{
						if (self::isDefault($dirProperty))
						{
							$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_PAGE_SECTION_DEFAULT", array(
								"#TITLE#" => htmlspecialchars($dirProperty),
								"#HREF#" => "/bitrix/admin/fileman_folder.php?lang=" . LANG . "&site=" . $arSite["LID"] . "&path=" . urlencode($arSite["DIR"]),
							));
						}
					}

				}
			}
		}
		return $arErrors;
	}

	/**
	 * Returns errors in pages settings
	 *
	 * @return string[]
	 */
	private static function checkPagesSettings()
	{
		$arErrors = array();
		$arSites = SiteTable::getList(array(
			"select" => array("LID", "DIR"),
		))->fetchAll();
		foreach ($arSites as $arSite)
		{
			$page = $arSite["DIR"] . "index.php";
			$content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . $page);
			$arMatches = array();
			$matches = preg_match_all('/\\$APPLICATION->set(.*)\\((.*)\\)/iu', $content, $arMatches);
			if ($matches)
			{
				for ($i = 0; $i < $matches; $i++)
				{
					$check = "";
					$property = "";
					if ($arMatches[1][$i] == "PageProperty")
					{
						$arTmp = explode(",", $arMatches[2][$i]);
						$property = $arTmp[0];
						$check = $arTmp[1];
					}
					elseif ($arMatches[1][$i] == "Title")
					{
						$check = $arMatches[2][$i];
						$property = "Title";
					}
					$property = trim(trim($property), "\"''");
					$check = trim(trim($check), "\"''");

					if ($check && self::isDefault($check))
					{
						if ($property == "Title")
						{
							$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_PAGE_TITLE_DEFAULT", array(
								"#TITLE#" => htmlspecialchars($check),
								"#HREF#" => "/bitrix/admin/fileman_file_view.php?lang=" . LANG . "&site=" . $arSite["LID"] . "&path=" . urlencode($page),
							));
						}
						else
						{
							$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES_PAGE_PROPERTY_DEFAULT", array(
								"#PROPERTY#" => htmlspecialchars($property),
								"#VALUE#" => htmlspecialchars($check),
								"#HREF#" => "/bitrix/admin/fileman_file_view.php?lang=" . LANG . "&site=" . $arSite["LID"] . "&path=" . urlencode($page),
							));
						}
					}
				}
			}
		}
		return $arErrors;
	}

	/**
	 * Returns true, if title seems to be default
	 *
	 * @param string $string title to test
	 *
	 * @return bool
	 */
	private static function isDefault($string)
	{
		$words = explode(", ", Loc::getMessage("intervolga.checklist.IV_DEFAULT_TITLES"));
		$defaultWords = 0;
		foreach ($words as $word)
		{
			$defaultWords += substr_count(strtoupper($string), strtoupper($word));
		}
		return $defaultWords > 0;
	}
}