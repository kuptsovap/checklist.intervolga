<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class ToDoComments extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_TODO_COMMENTS";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_TODO_COMMENTS_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_TODO_COMMENTS_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array();
		$dir = new Directory(Application::getDocumentRoot()."/modules");
		$dir1 = new Directory(Application::getDocumentRoot()."/bitrix/templates");
		$dir2 = new Directory(Application::getDocumentRoot()."/bitrix/php_interface");
		$dir3 = new Directory(Application::getDocumentRoot()."/bitrix/components");
		if ($arFiles = array_merge(parent::getTestFiles($dir,true),parent::getTestFiles($dir1,true),parent::getTestFiles($dir2,true),parent::getTestFiles($dir3,true)))
		{
			foreach ($arFiles as $file)
			{
				$content = $file->getContents();
				if ($content)
				{
					$arErrors = array_merge($arErrors, static::checkContent($content, $file));
				}
			}
		}
		
		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.OK");
		}
		return $arResult;
	}
	
	protected static function checkContent($content, $file)
	{
		$arErrors = array();

		if ($todo = substr_count($content, "TODO"))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.TODO_FOUND", array(
				"#CNT#" => $todo,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}

		return $arErrors;
	}
}