<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class DebugingPrinting extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_DEBUG_PRINTING";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_DEBUG_PRINTING_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_DEBUG_PRINTING_TEST_DESC");
	}
	
	public static function run()
	{
		return parent::run();
	}

	protected static function checkContent($content, $file)
	{
		$arErrors = array();

		if ($printr = substr_count($content, "print_r("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.PRINTR_FOUND", array(
				"#CNT#" => $printr,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($vardump = substr_count($content, "var_dump("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.VARDUMP_FOUND", array(
				"#CNT#" => $vardump,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($varexport = substr_count($content, "var_export("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.VAREXPORT_FOUND", array(
				"#CNT#" => $varexport,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($dump = substr_count($content, "Debug::dump("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.DEBUGDUMP_FOUND", array(
				"#CNT#" => $dump,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($writetofile = substr_count($content, "Debug::writeToFile("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.DEBUGWRITETOFILE_FOUND", array(
				"#CNT#" => $writetofile,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}
		if ($dumptofile = substr_count($content, "Debug::dumpToFile("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.DEBUGDUMPTOFILE_FOUND", array(
				"#CNT#" => $dumptofile,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}

		return $arErrors;
	}
}