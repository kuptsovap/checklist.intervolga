<?namespace Intervolga\Checklist\Tests;

use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class DisabledEntities extends Base
{
	public static function getCode()
	{
		return "IV_DISABLED_ENTITIES";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array_merge(self::checkDisabledIblocks(), self::checkDisabledProperties());

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_OK");
		}

		return $arResult;
	}

	/**
	 * Returns errors with disabled iblocks
	 *
	 * @return string[]
	 */
	private static function checkDisabledIblocks()
	{
		$arErrors = array();
		if (Loader::includeModule("iblock"))
		{
			$dbIblocks = IblockTable::getList(array(
				"filter" => array("ACTIVE" => "N"),
				"order" => array("NAME" => "ASC"),
				"select" => array("ID", "NAME", "IBLOCK_TYPE_ID"),
			));
			while ($arIblock = $dbIblocks->fetch())
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_IBLOCK", array(
					"#ID#" => $arIblock["ID"],
					"#NAME#" => $arIblock["NAME"],
					"#HREF#" => "/bitrix/admin/iblock_admin.php?type=" . $arIblock["IBLOCK_TYPE_ID"] . "&lang=" . LANG . "&admin=Y"
				));
			}
		}
		return $arErrors;
	}

	/**
	 * Returns errors with disabled properties
	 *
	 * @return string[]
	 */
	private static function checkDisabledProperties()
	{
		$arErrors = array();
		if (Loader::includeModule("iblock"))
		{
			$dbProperties = PropertyTable::getList(array(
				"filter" => array("ACTIVE" => "N"),
				"order" => array("IBLOCK.NAME" => "ASC", "NAME" => "ASC"),
				"select" => array("ID", "NAME", "IBLOCK.ID", "IBLOCK.NAME", "IBLOCK.IBLOCK_TYPE_ID")
			));
			while ($arProperty = $dbProperties->fetch())
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_DISABLED_ENTITIES_PROPERTY", array(
					"#ID#" => $arProperty["ID"],
					"#NAME#" => $arProperty["NAME"],
					"#IBLOCK_ID#" => $arProperty["IBLOCK_PROPERTY_IBLOCK_ID"],
					"#IBLOCK_NAME#" => $arProperty["IBLOCK_PROPERTY_IBLOCK_NAME"],
					"#HREF#" => "/bitrix/admin/iblock_edit.php?ID=" . $arProperty["IBLOCK_PROPERTY_IBLOCK_ID"] . "&type=" . $arProperty["IBLOCK_PROPERTY_IBLOCK_IBLOCK_TYPE_ID"] . "&lang=" . LANG . "&admin=Y"
				));
			}
		}

		return $arErrors;
	}
}