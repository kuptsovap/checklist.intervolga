<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class GetList extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_GET_LIST";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_GET_LIST_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_GET_LIST_TEST_DESC");
	}

	public static function run()
	{
		return parent::run();
	}
	
	protected static function checkContent($content, $file)
	{
		$arErrors = array();

		if ($getlist = substr_count($content, "::GetList("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.GETLIST_FOUND", array(
				"#CNT#" => $getlist,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}

		return $arErrors;
	}
}