<?namespace Intervolga\Checklist\Tests;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class IblockRights extends Base
{
	public static function getCode()
	{
		return "IV_IBLOCK_RIGHTS";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_IBLOCK_RIGHTS_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_IBLOCK_RIGHTS_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = self::checkRights();

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_IBLOCK_RIGHTS_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_IBLOCK_RIGHTS_OK");
		}

		return $arResult;
	}

	/**
	 * Returns errors with iblock rights
	 *
	 * @return string[]
	 */
	public static function checkRights()
	{
		$arErrors = array();

		if (Loader::includeModule("iblock"))
		{
			$dbIblocks = IblockTable::getList(array(
				"filter" => array(
					array(
						"LOGIC" => "OR",
						array("!LIST_PAGE_URL" => false),
						array("!DETAIL_PAGE_URL" => false),
						array("!SECTION_PAGE_URL" => false),
					)
				),
				"select" => array("ID", "NAME", "RIGHTS_MODE", "LIST_PAGE_URL", "DETAIL_PAGE_URL", "SECTION_PAGE_URL", "IBLOCK_TYPE_ID")
			));
			while ($arIblock = $dbIblocks->fetch())
			{
				if ($arIblock["RIGHTS_MODE"] == "S")
				{
					$arRights = \CIBlock::GetGroupPermissions($arIblock["ID"]);
					if (!$arRights[2] || $arRights[2] == "D")
					{
						$arErrors[] = Loc::getMessage("intervolga.checklist.IV_IBLOCK_RIGHTS_ERROR", array(
							"#ID#" => $arIblock["ID"],
							"#NAME#" => $arIblock["NAME"],
							"#HREF#" => "/bitrix/admin/iblock_edit.php?ID=" . $arIblock["ID"] . "&type=" . $arIblock["IBLOCK_TYPE_ID"] . "&lang=" . LANG . "&admin=Y"
						));
					}
				}
			}
		}
		return $arErrors;
	}
}