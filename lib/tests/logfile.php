<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class LogFile extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_LOGFILE";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_LOGFILE_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_LOGFILE_DESC");
	}

	public static function run()
	{
		$arResult = array( "STATUS" => true);
		$arErrors = array();
				
		if (defined("LOG_FILENAME")==true) 
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.CONSTANT_EXIST", array(
						"#NAME#" => LOG_FILENAME
					));
			
			if(substr_count(LOG_FILENAME,"log.txt"))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.SIMPLE_NAME");
			}				
		}
		
		$dir = new Directory(Application::getDocumentRoot());
		if ($arFiles = self::getTestFiles($dir,true,"txt"))
		{
			foreach ($arFiles as $file)
			{
				if (!strcmp($file->getName(),"log.txt"))
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.LOGFILE_FOUND", array(
						"#PAGE#" => $file->getPhysicalPath()
						));	
				}
			}
		}
		
		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = static::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.OK");
		}
		return $arResult;
	}

}