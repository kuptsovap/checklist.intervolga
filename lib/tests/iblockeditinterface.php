<?namespace Intervolga\Checklist\Tests;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class IblockEditInterface extends Base
{
	public static function getCode()
	{
		return "IV_IBLOCK_EDIT_INTERFACE";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		if (Loader::includeModule("iblock"))
		{
			if ($arIblocks = self::getIblocks())
			{
				// Get all iblocks form settings
				$arFormOptions = array();
				$dbOptions = \CUserOptions::GetList(array(), array("CATEGORY" => "form", "COMMON" => "Y",));
				while ($arOption = $dbOptions->Fetch())
				{
					$arFormOptions[$arOption["NAME"]] = $arOption;
				}

				$arErrors = array();

				foreach ($arIblocks as $arIblock)
				{
					if (!$arFormOptions["form_element_" . $arIblock["ID"]])
					{
						$arErrors[] = Loc::getMessage("intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_ERROR", array(
							"#HREF#" => self::getIblockElementsUrl($arIblock),
							"#IBLOCK#" => $arIblock["NAME"],
							"#ID#" => $arIblock["ID"],
						));
					}
				}
				if ($arErrors)
				{
					$arResult["STATUS"] = false;
					$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
					$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
				}
			}
		}
		return $arResult;
	}

	/**
	 * Returns all iblocks with general settings, need for building url
	 *
	 * @return array [ID] => iblock
	 */
	private static function getIblocks()
	{
		$arIblocks = array();
		$defaultListMode = (Option::get("iblock", "combined_list_mode") == "Y" ? "C" : "S");

		$dbIblocks = IblockTable::getList(
			array(
				"select" => array("ID", "NAME", "IBLOCK_TYPE_ID", "LIST_MODE", "TYPE.SECTIONS"),
				"order" => array("ID" => "ASC"),
			)
		);
		while ($arIblock = $dbIblocks->Fetch())
		{
			if (!$arIblock["LIST_MODE"])
			{
				$arIblock["LIST_MODE"] = $defaultListMode;
			}
			$arIblocks[$arIblock["ID"]] = $arIblock;
		}
		return $arIblocks;
	}

	/**
	 * Returns url to iblock element list
	 *
	 * @param array $arIblock iblocks
	 *
	 * @return string
	 */
	private static function getIblockElementsUrl($arIblock)
	{
		if ($arIblock["IBLOCK_IBLOCK_TYPE_SECTIONS"] == "N" || $arIblock["LIST_MODE"] == "C")
		{
			return "/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=" . $arIblock["ID"] . "&type=" . $arIblock["IBLOCK_TYPE_ID"] . "&lang=" . LANG;
		}
		else
		{
			return "/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=" . $arIblock["ID"] . "&type=" . $arIblock["IBLOCK_TYPE_ID"] . "&lang=" . LANG . "&find_el_y=Y";
		}
	}
}