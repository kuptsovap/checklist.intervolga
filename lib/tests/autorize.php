<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Authorize extends PhpFileBase
{
	public static function getCode()
	{
		return "IV_AUTHORIZE";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_AUTHORIZE_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_AUTHORIZE_TEST_DESC");
	}
	
	public static function run()
	{
		return parent::run();
	}

	protected static function checkContent($content, $file)
	{
		$arErrors = array();

		if ($authorize = substr_count($content, "->authorize("))
		{
			$arErrors[] = Loc::getMessage("intervolga.checklist.AUTHORIZE_FOUND", array(
				"#CNT#" => $authorize,
				"#PAGE#" => $file->getPhysicalPath()
			));
		}

		return $arErrors;
	}
}