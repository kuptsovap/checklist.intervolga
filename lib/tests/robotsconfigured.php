<?namespace Intervolga\Checklist\Tests;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class RobotsConfigured extends Base
{
	public static function getCode()
	{
		return "IV_ROBOTS_CONFIGURED";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = self::checkSites();

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_OK");
		}

		return $arResult;
	}

	/**
	 * Checks each site's robots.txt
	 *
	 * @return string[]
	 */
	private static function checkSites()
	{
		$arErrors = array();
		$dbSites = SiteTable::getList();
		while ($arSite = $dbSites->fetch())
		{
			$robotsPath = ($arSite["DOC_ROOT"] ? $arSite["DOC_ROOT"] : $_SERVER["DOCUMENT_ROOT"]) . $arSite["DIR"] . "robots.txt";
			if (file_exists($robotsPath))
			{
				$robots = file_get_contents($robotsPath);
				$arCommands = array_diff(explode("\n", $robots), array(""));
				foreach ($arCommands as $i => $command)
				{
					$arCommands[$i] = trim($command);
				}

				if (!in_array("Disallow: /bitrix/", $arCommands))
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_BITRIX_NOT_EXCLUDED", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
				}

				if (!in_array("Disallow: /", $arCommands) && substr_count($robots, "Host:") == 0)
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_NOT_CONFIGURED", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
				}
			}
			else
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_ROBOTS_CONFIGURED_NOT_FOUND", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
			}
		}
		return $arErrors;
	}
}