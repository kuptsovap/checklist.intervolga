<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class Favicon extends Base
{
	public static function getCode()
	{
		return "IV_FAVICON";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_FAVICON_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_FAVICON_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = self::checkFavicon();

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_FAVICON_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_FAVICON_OK");
		}

		return $arResult;
	}

	/**
	 * Checks each site's favicon.io
	 *
	 * @return string[]
	 */
	private static function checkFavicon()
	{
		$arErrors = array();
		$dbSites = SiteTable::getList();
		while ($arSite = $dbSites->fetch())
		{
			$faviconPath = ($arSite["DOC_ROOT"] ? $arSite["DOC_ROOT"] : $_SERVER["DOCUMENT_ROOT"]) . $arSite["DIR"] . 'favicon.ico';
			if (!file_exists($faviconPath))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_FAVICON_FILE_NOT_FOUND", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
			}
			else
			{
				$deffavicon = file_get_contents($_SERVER["DOCUMENT_ROOT"] . $arSite["DIR"] . 'bitrix/modules/intervolga.checklist/favicon.ico');
				$favicon = file_get_contents($faviconPath);
				if (md5($deffavicon) === md5($favicon))
				{
					$arErrors[] = Loc::getMessage("intervolga.checklist.EQUAL", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
				}
			}
		}
		return $arErrors;
	}
}