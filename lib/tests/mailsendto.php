<?namespace Intervolga\Checklist\Tests;

use Bitrix\Main\Config\Option;
use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Mail\Internal\EventMessageTable;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class MailSendTo extends Base
{
	public static function getCode()
	{
		return "IV_MAIL_SEND_TO";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = array_merge(self::getMessagesErrors(), self::getSettingsErrors(), self::getSiteErrors());

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}

		return $arResult;
	}

	/**
	 * Returns site settings errors
	 *
	 * @return string[]
	 */
	private static function getSiteErrors()
	{
		$arErrors = array();

		$dbSites = SiteTable::getList(array(
			"order" => array("LID" => "ASC"),
			"select" => array("LID", "EMAIL", "SITE_NAME", "NAME")
		));

		while ($arSite = $dbSites->fetch())
		{
			if (self::isIntervolgaEmail($arSite["EMAIL"]))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_SITE_ERROR", array(
					"#HREF#" => "/bitrix/admin/site_edit.php?lang=" . LANG . "&LID=" . $arSite["LID"],
					"#NAME#" => $arSite["NAME"],
					"#LID#" => $arSite["LID"],
				));
			}
		}

		return $arErrors;
	}

	/**
	 * Returns module settings errors
	 *
	 * @return string[]
	 */
	private static function getSettingsErrors()
	{
		$arErrors = array();
		if ($email = Option::get("main", "all_bcc"))
		{
			if (self::isIntervolgaEmail($email))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_ALL_BCC_ERROR", array(
					"#HREF#" => "/bitrix/admin/settings.php?lang=" . LANG . "&mid=main",
				));
			}
		}
		if ($email = Option::get("main", "email_from"))
		{
			if (self::isIntervolgaEmail($email))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_EMAIL_FROM_ERROR", array(
					"#HREF#" => "/bitrix/admin/settings.php?lang=" . LANG . "&mid=main",
				));
			}
		}
		if ($email = Option::get("sale", "order_email"))
		{
			if (self::isIntervolgaEmail($email))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_ORDER_EMAIL_ERROR", array(
					"#HREF#" => "/bitrix/admin/settings.php?lang=" . LANG . "&mid=sale",
				));
			}
		}

		return $arErrors;
	}

	/**
	 * Returns test errors in mail messages
	 *
	 * @return string[]
	 */
	private static function getMessagesErrors()
	{
		$arErrors = array();

		$dbMessages = EventMessageTable::getList(array(
			"select" => array("ID", "EVENT_NAME", "SUBJECT", "EMAIL_TO", "EMAIL_FROM"),
			"order" => array("ID" => "ASC"),
		));
		while ($arMessage = $dbMessages->fetch())
		{
			if (self::isIntervolgaEmail($arMessage["EMAIL_TO"]) || self::isIntervolgaEmail($arMessage["EMAIL_FROM"]))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_MAIL_SEND_TO_ERROR_MESSAGE", array(
					"#HREF#" => "/bitrix/admin/message_edit.php?lang=" . LANG . "&ID=" . $arMessage["ID"],
					"#EVENT_NAME#" => $arMessage["EVENT_NAME"],
					"#SUBJECT#" => $arMessage["SUBJECT"],
				));
			}
		}

		return $arErrors;
	}

	/**
	 * Checks if email belongs to intervolga.ru domain
	 *
	 * @param string $email
	 *
	 * @return bool
	 */
	private static function isIntervolgaEmail($email)
	{
		return substr_count($email, "@intervolga.ru") > 0;
	}
}