<?namespace Intervolga\Checklist\Tests;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SiteTable;

Loc::loadMessages(__FILE__);

class Page404 extends Base
{	
	public static function getCode()
	{
		return "IV_404_PAGE";
	}

	public static function getName()
	{
		return Loc::getMessage("intervolga.checklist.IV_404_PAGE_TEST_NAME");
	}

	public static function getDescription()
	{
		return Loc::getMessage("intervolga.checklist.IV_404_PAGE_TEST_DESC");
	}

	public static function run()
	{
		$arResult = parent::run();
		$arErrors = self::checkSites();

		if ($arErrors)
		{
			$arResult["STATUS"] = false;
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_404_PAGE_ERRORS_FOUND", array("#CNT#" => count($arErrors)));
			$arResult["MESSAGE"]["DETAIL"] = self::formatErrorsList($arErrors);
		}
		else
		{
			$arResult["MESSAGE"]["PREVIEW"] = Loc::getMessage("intervolga.checklist.IV_404_PAGE_OK");
		}

		return $arResult;
	}

	/**
	 * Checks each site's 404.php
	 *
	 * @return string[]
	 */
	private static function checkSites()
	{
		$arErrors = array();
		$dbSites = SiteTable::getList();
		while ($arSite = $dbSites->fetch())
		{
			$page404Path = ($arSite["DOC_ROOT"] ? $arSite["DOC_ROOT"] : $_SERVER["DOCUMENT_ROOT"]) . $arSite["DIR"] . '404.php';
			if (!file_exists($page404Path))
			{
				$arErrors[] = Loc::getMessage("intervolga.checklist.IV_404_PAGE_NOT_FOUND", array("#LID#" => $arSite["LID"], "#NAME#" => $arSite["NAME"]));
			}
		}
		return $arErrors;
	}
}