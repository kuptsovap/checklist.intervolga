<?namespace Intervolga\Checklist\Tests;

abstract class Base
{
	/**
	 * Returns test's parent category code
	 *
	 * @return string
	 */
	public static function getParent()
	{
		return "IV_BASE";
	}

	/**
	 * Returns true if test is required
	 *
	 * @return bool
	 */
	public static function isRequired()
	{
		return true;
	}

	/**
	 * Returns true if test is automated
	 *
	 * @return bool
	 */
	public static function isAuto()
	{
		return true;
	}

	/**
	 * Returns test code
	 *
	 * @return string
	 */
	public static function getCode()
	{
		return get_called_class();
	}

	/**
	 * Returns test name
	 *
	 * @return string
	 */
	public static function getName()
	{
		return get_called_class();
	}

	/**
	 * Returns test description
	 *
	 * @return string
	 */
	public static function getDescription()
	{
		return "TODO: Desctiption " . get_called_class();
	}

	/**
	 * Returns test how-to text
	 *
	 * @return string
	 */
	public static function getHowTo()
	{
		return "";
	}

	/**
	 * Returns test links
	 *
	 * @return string
	 */
	public static function getLinks()
	{
		return "";
	}

	/**
	 * Returns checklist test description
	 *
	 * @return array
	 */
	public static function getCheckListPoint()
	{
		$arCheckListPoint = array(
			"PARENT" => static::getParent(),
			"NAME" => static::getName(),
			"CLASS_NAME" => get_called_class(),
			"METHOD_NAME" => "run",
		);
		if (static::isRequired())
		{
			$arCheckListPoint["REQUIRED"] = "Y";
		}
		if (static::isAuto())
		{
			$arCheckListPoint["AUTO"] = "Y";
		}
		if (static::getDescription())
		{
			$arCheckListPoint["DESC"] = static::getDescription();
		}
		if (static::getHowTo())
		{
			$arCheckListPoint["HOWTO"] = static::getHowTo();
		}
		if (static::getLinks())
		{
			$arCheckListPoint["LINKS"] = static::getLinks();
		}
		return $arCheckListPoint;
	}

	/**
	 * Returns test result
	 *
	 * @return array
	 */
	public static function run()
	{
		return array("STATUS" => true);
	}

	/**
	 * Formats array of errors to ordered list
	 *
	 * @param string[] $arErrors errors list
	 *
	 * @return string ordered list of errors
	 */
	public static function formatErrorsList(array $arErrors)
	{
		if ($arErrors)
		{
			foreach ($arErrors as $i => $error)
			{
				$arErrors[$i] = "<li>" .$error . "</li>";
			}
		}

		return "<ol>" . implode("", $arErrors) . "</ol>";
	}
}