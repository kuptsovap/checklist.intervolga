<?
$MESS["intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_NAME"] = "Интерфейсы редактирования инфоблоков настроены";
$MESS["intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_DESC"] = "Форма редактирования каждого инфоблока должна быть настроена индивидуально с учетом тех данных, которые действительно используются на сайте. Все неиспользуемые поля должны быть скрыты.";
$MESS["intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_ERRORS_FOUND"] = "Найдены инфоблоки без настроек интерфейса редактирования (#CNT#)";
$MESS["intervolga.checklist.IBLOCK_EDIT_INTERFACE_TEST_ERROR"] = "Интерфейс редактирования инфоблока <a href=\"#HREF#\" target=\"_blank\">[#ID#] &laquo;#IBLOCK#&raquo;</a> не настроен";