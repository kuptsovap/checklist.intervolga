<?
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_TEST_NAME"] = "robots.txt создан и настроен";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_TEST_DESC"] = "Файл robots.txt в корне сайтов создан и настроен.<br>Корректный robots.txt должен содержать либо инструкцию Disallow: / (быть закрытым от индексации), либо иметь установленный Host.<br>Так же обязательно должна быть инструкция для исключения файлов Битрикса из индекса (Disallow: /bitrix/)";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_OK"] = "robots.txt создан и настроен";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_NOT_FOUND"] = "Файл robots.txt не найден в корневой папке сайта [#LID#] &laquo;#NAME#&raquo;";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_NOT_CONFIGURED"] = "На сайте [#LID#] &laquo;#NAME#&raquo; (нет ни Disallow: /, ни Host)";
$MESS["intervolga.checklist.IV_ROBOTS_CONFIGURED_BITRIX_NOT_EXCLUDED"] = "Каталог /bitrix/ не был исключен на сайте [#LID#] &laquo;#NAME#&raquo;";
