﻿<?
$MESS["intervolga.checklist.IV_DEBUG_PRINTING_TEST_NAME"] = "Удалена отладочная печать из кода";
$MESS["intervolga.checklist.IV_DEBUG_PRINTING_TEST_DESC"] = "В коде не используется отладочная печать - print_r, var_dump, var_export";
$MESS["intervolga.checklist.PRINTR_FOUND"] = "В файле #PAGE# найдена функция print_r (#CNT# шт)";
$MESS["intervolga.checklist.VARDUMP_FOUND"] = "В файле #PAGE# найдена функция var_dump (#CNT# шт)";
$MESS["intervolga.checklist.VAREXPORT_FOUND"] = "В файле #PAGE# найдена функция var_export (#CNT# шт)";
$MESS["intervolga.checklist.DEBUGDUMP_FOUND"] = "В файле #PAGE# найдена функция Debug::dump (#CNT# шт)";
$MESS["intervolga.checklist.DEBUGDUMPTOFILE_FOUND"] = "В файле #PAGE# найдена функция Debug::dumpToFile (#CNT# шт)";
$MESS["intervolga.checklist.DEBUGWRITETOFILE_FOUND"] = "В файле #PAGE# найдена функция Debug::writeToFile (#CNT# шт)";
