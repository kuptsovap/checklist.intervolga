﻿<?
$MESS["intervolga.checklist.IV_DISPLAY_ERROR_TEST_NAME"] = "Значение константы display_error";
$MESS["intervolga.checklist.IV_DISPLAY_ERROR_TEST_DESC"] = "Константа display_error выставлена в 'on'";
$MESS["intervolga.checklist.IV_DISPLAY_ERROR_ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.IV_DISPLAY_ERROR_OK"] = "Константа имеет значение 'on'";
$MESS["intervolga.checklist.DISPLAY_OFF"] = "В файле #PAGE# константа display_error выставлена в 'off'";
