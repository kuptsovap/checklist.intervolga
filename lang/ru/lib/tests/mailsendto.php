<?
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_TEST_NAME"] = "Сотрудники ИНТЕРВОЛГИ не получают почту и не отправляют почту с сайта";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_TEST_DESC"] = "В качестве получателей и отправителей писем с сайта должны быть указаны почтовые ящики без @intervolga.ru";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_ERROR_MESSAGE"] = "Проверьте почтовый шаблон <a href=\"#HREF#\" target=\"_blank\">[#EVENT_NAME#] &laquo;#SUBJECT#&raquo;</a>";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_ERRORS_FOUND"] = "Найдены почтовые ящики сотрудников ИНТЕРВОЛГИ (#CNT#)";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_ALL_BCC_ERROR"] = "Проверьте настройку модуля <a href=\"#HREF#\" target=\"_blank\">&laquo;E-Mail (или список через запятую), на который будут дублироваться исходящие сообщения&raquo;</a>";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_EMAIL_FROM_ERROR"] = "Проверьте настройку модуля <a href=\"#HREF#\" target=\"_blank\">&laquo;E-Mail администратора сайта (отправитель по умолчанию)&raquo;</a>";
$MESS["intervolga.checklist.IV_MAIL_SEND_TO_ORDER_EMAIL_ERROR"] = "Проверьте настройку модуля <a href=\"#HREF#\" target=\"_blank\">&laquo;E-Mail отдела продаж&raquo;</a>";
$MESS["intervolga.checklist.IV_MAIL_SEND_SITE_ERROR"] = "Проверьте настройки сайта <a href=\"#HREF#\" target=\"_blank\">[#LID#] &laquo;#NAME#&raquo;</a>";