﻿<?
$MESS["intervolga.checklist.IV_LOGFILE_NAME"] = "Проверка состояния константы LOG_FILENAME и файлов лога";
$MESS["intervolga.checklist.IV_LOGFILE_DESC"] = "Константа не должна быть определена, если определена, то ее имя не - log.txt, не должно быть файлов log.txt на сайте";
$MESS["intervolga.checklist.CONSTANT_EXIST"] = "Константа LOG_FILENAME определена - #NAME#";
$MESS["intervolga.checklist.SIMPLE_NAME"] = "Константа LOG_FILENAME имеет простое имя log.txt";
$MESS["intervolga.checklist.LOGFILE_FOUND"] = "Найден файл #PAGE#";
$MESS["intervolga.checklist.ERRORS_FOUND"] = "Найдены ошибки (#CNT# шт)";
$MESS["intervolga.checklist.OK"] = "Константа не определена и нет файлов лога на сайте";