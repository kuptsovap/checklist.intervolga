<?
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_TEST_NAME"] = "Изображения на главной странице оптимизированы";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_TEST_DESC"] = "Изображения на главной странице должны весить < 2 МБ";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_OK"] = "Изображения на главной странице оптимизированы";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_TOO_LARGE"] = "Изображения на странице <a href=\"#URL#\" target=\"_blank\">#PAGE#</a> не оптимизированы (найдено изображений: #COUNT#, общий размер: #SIZE#)";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_FITS"] = "Изображения на странице <a href=\"#URL#\" target=\"_blank\">#PAGE#</a> оптимизированы (найдено изображений: #COUNT#, общий размер: #SIZE#)";
$MESS["intervolga.checklist.IV_PUBLIC_IMAGES_NO_IMAGES"] = "Изображения на странице <a href=\"#URL#\" target=\"_blank\">#PAGE#</a> не обнаружены";