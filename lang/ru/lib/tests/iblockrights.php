<?
$MESS["intervolga.checklist.IV_IBLOCK_RIGHTS_TEST_NAME"] = "Права на инфоблоки";
$MESS["intervolga.checklist.IV_IBLOCK_RIGHTS_TEST_DESC"] = "Инфоблоки, для которых прописаны шаблоны URL'ов, должны быть доступны не только админам";
$MESS["intervolga.checklist.IV_IBLOCK_RIGHTS_ERROR"] = "Нужно открыть доступ к инфоблоку <a href=\"#HREF#\" target=\"_blank\">[#ID#] &laquo;#NAME#&raquo;</a>";
$MESS["intervolga.checklist.IV_IBLOCK_RIGHTS_ERRORS_FOUND"] = "Обнаружены проблемы с правами доступа к инфоблокам (#CNT#)";
$MESS["intervolga.checklist.IV_IBLOCK_RIGHTS_OK"] = "Права к инфоблокам настроены корректно";