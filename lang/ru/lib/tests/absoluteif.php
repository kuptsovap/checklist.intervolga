﻿<?
$MESS["intervolga.checklist.IV_ABSOLUTE_IF_TEST_NAME"] = "Нет абсолютных проверок в коде - if(0),if(1),if(true),if(false)";
$MESS["intervolga.checklist.IV_ABSOLUTE_IF_TEST_DESC"] = "В коде не используются абсолютные провери - if(0),if(1),if(true),if(false)";
$MESS["intervolga.checklist.IF0_FOUND"] = "В файле #PAGE# найдена проверка if(0) (#CNT# шт)";
$MESS["intervolga.checklist.IF1_FOUND"] = "В файле #PAGE# найдена проверка if(1) (#CNT# шт)";
$MESS["intervolga.checklist.IFTRUE_FOUND"] = "В файле #PAGE# найдена проверка if(true) (#CNT# шт)";
$MESS["intervolga.checklist.IFFALSE_FOUND"] = "В файле #PAGE# найдена проверка if(false) (#CNT# шт)";