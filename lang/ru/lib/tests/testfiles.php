<?
$MESS["intervolga.checklist.IV_TEST_FILES_TEST_NAME"] = "Удалены служебные файлы";
$MESS["intervolga.checklist.IV_TEST_FILES_TEST_DESC"] = "В файловой структуре сайта нет файлов и разделов с названиями test, demo, hidden и т.п.";
$MESS["intervolga.checklist.IV_TEST_FILES_ERRORS_FOUND"] = "Найдены файлы, похожие на служебные (#CNT#)";
$MESS["intervolga.checklist.IV_TEST_FILES_DIR_ERROR"] = "Раздел <a href=\"#HREF#\" target=\"_blank\">&laquo;#DIR#&raquo;</a> может содержать служебные файлы";
$MESS["intervolga.checklist.IV_TEST_FILES_FILE_ERROR"] = "Файл <a href=\"#HREF#\" target=\"_blank\">&laquo;#FILE#&raquo;</a> может быть служебным";