﻿<?
$MESS["intervolga.checklist.IV_FAVICON_TEST_NAME"] = "Проверка фавикона";
$MESS["intervolga.checklist.IV_FAVICON_TEST_DESC"] = "Есть фавикон, и он отличается от нашего стандартного";
$MESS["intervolga.checklist.IV_FAVICON_ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.IV_FAVICON_OK"] = "Фавикон есть, он отличается от стандартного";
$MESS["intervolga.checklist.IV_FAVICON_FILE_NOT_FOUND"] = "Фавикон не найдена в корневой папке сайта [#LID#] &laquo;#NAME#&raquo;";
$MESS["intervolga.checklist.EQUAL"] = "Фавикон в корневой папаке сайта [#LID#] &laquo;#NAME#&raquo; не был изменен";
