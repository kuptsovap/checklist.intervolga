﻿<?
$MESS["intervolga.checklist.IV_404_PAGE_TEST_NAME"] = "Страница 404.php создана";
$MESS["intervolga.checklist.IV_404_PAGE_TEST_DESC"] = "Страница 404.php в корне сайтов создана.";
$MESS["intervolga.checklist.IV_404_PAGE_ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.IV_404_PAGE_OK"] = "Страница 404.php создана";
$MESS["intervolga.checklist.IV_404_PAGE_NOT_FOUND"] = "Страница 404.php не найдена в корневой папке сайта [#LID#] &laquo;#NAME#&raquo;";
