<?
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_TEST_NAME"] = "Установлено название сайта";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_TEST_DESC"] = "В модуле, настройках сайтов и на главной странице установлено реальное название сайта";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_ERRORS_FOUND"] = "Найдены ошибки настройки названия сайта (#CNT#)";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_OK"] = "Название сайта установлено";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_MODULE_SITE_NAME_DEFAULT"] = "Похоже, название сайта в <a href=\"#HREF#\" target=\"_blank\">настройках главного модуля</a> не было изменено: &laquo;#TITLE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_SITE_NAME_DEFAULT"] = "Похоже, название в <a href=\"#HREF#\" target=\"_blank\">настройках сайта [#LID#]</a> не было изменено: &laquo;#TITLE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_MODULE_SITE_SITE_NAME_DEFAULT"] = "Похоже, название сайта в <a href=\"#HREF#\" target=\"_blank\">настройках сайта [#LID#]</a> не было изменено: &laquo;#TITLE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_PAGE_SECTION_DEFAULT"] = "Похоже, название сайта в <a href=\"#HREF#\" target=\"_blank\">настройках главного раздела</a> не было изменено: &laquo;#TITLE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_PAGE_PROPERTY_DEFAULT"] = "Похоже, свойство &laquo;#PROPERTY#&raquo; <a href=\"#HREF#\" target=\"_blank\">главной страницы</a> не было изменено: &laquo;#VALUE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES_PAGE_TITLE_DEFAULT"] = "Похоже, заголовок <a href=\"#HREF#\" target=\"_blank\">главной страницы</a> не был изменен: &laquo;#TITLE#&raquo;";
$MESS["intervolga.checklist.IV_DEFAULT_TITLES"] = "bitrix, битрикс, bootstrap, бутстрап, управление, сайтом, демо, manager";