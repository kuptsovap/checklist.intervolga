﻿<?
$MESS["intervolga.checklist.IV_TODO_COMMENTS_NAME"] = "Проверка на TODO-комментарии";
$MESS["intervolga.checklist.IV_TODO_COMMENTS_DESC"] = "В коде не должно быть TODO-комментарииев";
$MESS["intervolga.checklist.TODO_FOUND"] = "В файле #PAGE# найдено использование todo-комментариев (#CNT# шт)";
$MESS["intervolga.checklist.ERRORS_FOUND"] = "Найдены ошибки (#CNT#)";
$MESS["intervolga.checklist.OK"] = "Ошибок в коде не обнаружено";
$MESS["intervolga.checklist.PAGE_UNAVAILABLE"] = "Файл #PAGE# недоступнен или пуст";