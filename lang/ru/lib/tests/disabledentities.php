<?
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_TEST_NAME"] = "Отсутствуют деактивированные инфоблоки, свойства";
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_TEST_DESC"] = "Отсутствуют деактивированные инфоблоки, свойства";
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_ERRORS_FOUND"] = "Найдены деактивированные сущности: #CNT#";
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_OK"] = "Нет деактивированныхы сущностей";
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_IBLOCK"] = "Обнаружен деактивированный инфоблок: <a href=\"#HREF#\" target=\"_blank\">[#ID#] &laquo;#NAME#&raquo;</a>";
$MESS["intervolga.checklist.IV_DISABLED_ENTITIES_PROPERTY"] = "Обнаружено деактивированное свойство [#ID#] &laquo;#NAME#&raquo; в инфоблоке <a href=\"#HREF#\" target=\"_blank\">[#IBLOCK_ID#] &laquo;#IBLOCK_NAME#&raquo;</a>";