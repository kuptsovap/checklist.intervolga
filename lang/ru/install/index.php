<?
$MESS["intervolga.checklist.MODULE_NAME"] = "Монитор качества ИНТЕРВОЛГИ";
$MESS["intervolga.checklist.MODULE_DESC"] = "Добавляет в монитор качества тесты, специфичные для компании ИНТЕРВОЛГА";
$MESS["intervolga.checklist.PARTNER_NAME"] = "ИНТЕРВОЛГА";
$MESS["intervolga.checklist.PARTNER_URI"] = "http://www.intervolga.ru";