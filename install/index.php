<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File; 

IncludeModuleLangFile(__FILE__);

class intervolga_checklist extends CModule
{
	
	public static function getModuleId()
	{
		return basename(dirname(__DIR__));
	}

	public function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_ID = self::getModuleId();
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage(self::getModuleId() . ".MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage(self::getModuleId() . ".MODULE_DESC");

		$this->PARTNER_NAME = GetMessage(self::getModuleId() . ".PARTNER_NAME");
		$this->PARTNER_URI = GetMessage(self::getModuleId() . ".PARTNER_URI");
	}

	public function DoInstall()
	{
		global $DB, $DBType, $APPLICATION;
		
		$this->InstallEvents();
		File::putFileContents(Application::getDocumentRoot() . "/bitrix/tools/intervolga.checklist/crawler.php",'
<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intervolga.checklist/tools/crawler.php";
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>');
		$this->runSQLscript("install");
		RegisterModule(self::getModuleId());
	}
	
	private function runSQLscript($scriptName) 
	{
		global $DB, $DBType, $APPLICATION;
		
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/intervolga.checklist/install/db/".$DBType."/".$scriptName.".sql");
		if($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
		}
	}

	public function DoUninstall()
	{
		UnRegisterModule(self::getModuleId());
		Directory::deleteDirectory(Application::getDocumentRoot() . "/bitrix/tools/intervolga.checklist/"); 
		$this->runSQLscript("uninstall");
		$this->UnInstallEvents();
	}

	public function InstallEvents()
	{
		/**
		 * @see \Intervolga\Checklist\EventHandlers::onCheckListGet()
		 */
		RegisterModuleDependences("main", "OnCheckListGet", self::getModuleId(), "\\Intervolga\\Checklist\\EventHandlers", "onCheckListGet");
		return TRUE;
	}

	public function UnInstallEvents()
	{
		/**
		 * @see \Intervolga\Checklist\EventHandlers::onCheckListGet()
		 */
		UnRegisterModuleDependences("main", "OnCheckListGet", self::getModuleId(), "\\Intervolga\\Checklist\\EventHandlers", "onCheckListGet");
		return TRUE;
	}
}
?>
