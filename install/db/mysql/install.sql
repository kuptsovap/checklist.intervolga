create table if not exists reports (
   code varchar(255) not null,
   name varchar(255) not null,
   description varchar(255) not null,
   parentName varchar(255) not null default 'IV_BASE',
   status char(1)  not null default 'N',
   preview varchar(255) not null,
   details TEXT,
   primary key (code));