<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();

$login= $request->getPost("login");
$password= $request->getPost("password");

$rsUser = CUser::GetByLogin($login);
if ($arUser = $rsUser->Fetch())
{
	if(strlen($arUser["PASSWORD"]) > 32)
	{
		$salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
		$db_password = substr($arUser["PASSWORD"], -32);
    }
    else
    {
		$salt = "";
		$db_password = $arUser["PASSWORD"];
    }
    $user_password =  md5($salt.$password);
    if ( $user_password != $db_password )
    {
		header ("HTTP/1.0 200 OK");
		echo "Пароль неверный";
		exit;
    }
}
else
{
	header ("HTTP/1.0 200 OK");
	echo "Такого логина не существует";
	exit;
}

$connection = Bitrix\Main\Application::getConnection();
$connection->query("DELETE FROM reports");
$reports = $request->getPost('reports');
foreach ($reports as $reportJSON)
{
	try 
	{
		$report = json_decode($reportJSON);
		$connection->query("INSERT INTO reports (code, name, description, status, preview, details)
		VALUES ('".$report->code."','".$report->name."','".$report->description."','".$report->status."','".$report->preview."','".$report->details."')");
	}
	catch(\Bitrix\Main\DB\SqlException $ex)
	{
		header ("HTTP/1.0 200 OK");
		echo "Произошла ошибка при записи в базу данных. Возможно, ваш код-отчета уже используется";
		exit;
	}
}
header ("HTTP/1.0 200 OK");
echo "Отчет успешно добавлен на сайт";
exit;
?>